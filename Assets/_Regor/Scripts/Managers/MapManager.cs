using System.Collections;
using System.Collections.Generic;
using TileWorld;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapManager : MonoBehaviour
{
    public static MapManager Instance { get; private set; }
    public byte TileSize;
    public Tilemap[] Tilemaps { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
        {
            Instance = this;
            Tilemaps = GetComponentsInChildren<Tilemap>();
        }
    }

    public void Start()
    {
        
    }
}